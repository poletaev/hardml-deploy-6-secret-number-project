from flask import Flask, jsonify
import signal
import sys
from time import sleep

import os

for name, value in os.environ.items():
    print("{0}: {1}".format(name, value))


secret_number = os.environ.get("SECRET_NUMBER")


def handler(sig, frame):
    print(f'YOU CALLED ME WITH {sig}')
    sleep(1)
    print('exit')
    sys.exit(sig)

# signal.signal(signal.SIGKILL, handler) # SIGKILL мы отловить не можем

signal.signal(signal.SIGTERM, handler)
signal.signal(signal.SIGINT, handler)
app = Flask(__name__)


@app.route('/')
def index():
    sleep(1)
    return "<span style='color:red'>Docker works!</span>"


@app.route('/healthcheck')
def check():
    return "ok"


@app.route("/return_secret_number")
def hello_world():
    sleep(1)
    return jsonify({"secret_number": secret_number})

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000)

#FROM tiangolo/uwsgi-nginx:python3.8-alpine
#FROM python:3.8-slim-buster
FROM python:3.8-alpine

WORKDIR /python-docker

COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

COPY . .

#STOPSIGNAL SIGINT

# final configuration
ENV FLASK_APP=app
ARG buildtime_secret=19
ENV SECRET_NUMBER=$buildtime_secret
EXPOSE 8000

ENTRYPOINT ["python3"]
CMD ["app_flask.py"]

